function Button({name}) {
    return ( 
        <button className="primary">{name}</button>
     );
}

export default Button;